[![Build Status](https://travis-ci.org/montao/gamex.svg?branch=master)](https://travis-ci.org/montao/gamex) [![Coverage Status](https://coveralls.io/repos/github/montao/gamex/badge.svg?branch=master)](https://coveralls.io/github/montao/gamex?branch=master)

# gamex
Simple Android 2D game. Instructions: Play with it. 

![](https://raw.githubusercontent.com/montao/gamex/master/jamie2d-Untitled.png)
